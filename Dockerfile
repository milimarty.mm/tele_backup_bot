FROM openjdk:15-jdk-alpine
EXPOSE 8080
RUN mkdir -p /app/
#ADD ./target/site-1.0.0.jar /app/site-1.0.0.jar
#VOLUME /tmp
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} /app/app.jar
ENTRYPOINT ["java","-jar","/app/com.backup_bot-1.0.jar"]