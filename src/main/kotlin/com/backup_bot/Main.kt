package com.backup_bot

import com.backup_bot.C.Companion.botInitStatus
import com.backup_bot.C.Companion.dbInitStatus
import com.backup_bot.C.Companion.queue
import com.backup_bot.bot.BotUtils
import com.backup_bot.user.UserDao
import com.backup_bot.user.UserModels
import org.apache.commons.io.FileUtils
import org.telegram.telegrambots.meta.api.objects.User
import org.zeroturnaround.zip.ZipUtil
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


fun main() {


    createScheduler()


    /**
     * initializer
     */
//   CoroutineScope

    Utils.init()
    Thread {
        while (true) {
            var msg: String?
            while (queue.poll().also {
                    msg = it
                    if (msg == "Bot") {
                        botInitStatus = true
                    }
                    if (msg == "DB") {
                        dbInitStatus = true
                    }
                } != null) {

                if (dbInitStatus && botInitStatus) {
                    switcher(0)
                    return@Thread
                }
            }

        }
    }.start()


}

fun switcher(select: Int) {
    when (select) {
        0 -> {
            while (true) {
                println("menu : \n 1- Create User \n 2- send Message With Bot \n 3- get all users \n 4- login")
                val x = readLine()!!.toInt()
                switcher(x)

            }
        }
        1 -> {

            val user: UserModels = UserModels()
            println("Create User")
            print("Email :")
            user.email = readLine().toString()
            print("Password :")
            user.password = readLine().toString()
            print("Role : ")
            user.role = readLine().toString()
            user.chatId = ""
            print("save it y/n?  ")
            val yon = readLine().toString()

            if (yon == "y") {
                UserDao.insertUser(user)
            }
            switcher(0)
        }
        2 -> {

            println("\r if you want back to menu press 0")
            print("message : ")
            val x = readLine()!!.toString()
            if (x == "0")
                switcher(0)
            print("cahtId : ")
            val chatId = readLine()
            BotUtils.sendMessage(x, chatId!!)
            switcher(2)
        }

        3 -> {

            print(UserDao.getAll {
                it?.forEach {
                    println(it)
                } ?: run {
                    println("No User Found")
                }
            })
            switcher(0)
        }
        4 -> {
            println("\r if you want back to menu press 0")
            print("Email : ")
            val email = readLine()!!.toString()
            if (email == "0")
                switcher(0)
            print("password : ")
            val pass = readLine()!!.toString()
            UserDao.getUserByEmail(email = email)?.let {
                if (pass == it.password) {
                    it.is_login = true

                } else
                    println("password wrong")
            }
            switcher(0)
        }
    }

}

fun createScheduler() {

    val scheduler: ScheduledExecutorService = Executors.newScheduledThreadPool(1)
    scheduler.scheduleAtFixedRate({
        println("---scheduled---")

//        var filePath = JBackupController().executeCommand(
//            "persian_loops",
//            "1234",
//            "backup",
//            File(System.getProperty("user.dir") + File.separator + "backups")
//        )
        val filePath2: String = "./app/backup/"
        val myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss")
        val myDateObj = LocalDateTime.now()
        val formattedDate: String = myDateObj.format(myFormatObj)
        print(formattedDate)
        val zipFileLocation = "E:\\Dev_Project\\Kotlin\\tele_db_backup_bot\\backup-$formattedDate.zip"
        val zipFile = File(zipFileLocation)
        try {
            zipFile.createNewFile()
            ZipUtil.pack(File(filePath2), zipFile);
        } catch (e: Exception) {
            e.printStackTrace()
        }
        println("create Zip File : $formattedDate ")

        UserDao.getUserByRole()?.let {
            it.forEach { t ->
                print(t)
                BotUtils.sendFile(
                    "This backup",
                    zipFileLocation,
                    t.chatId
                )
            }
        }


    },1 , 300, TimeUnit.MINUTES)

}



