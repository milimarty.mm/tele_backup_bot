package com.backup_bot.user

import com.backup_bot.db.DbUtils
import org.hibernate.query.Query


object UserDao {


    fun getUserByChatId(commit: Boolean = true, chatId: String): UserModels? {
        var q: Query<UserModels>? = null
        DbUtils.transaction(commit) {
            val user = DbUtils.GetQ(UserModels::class.java)
            user.query.select(user.root).where(
                user.builder.equal(user.root.get<Any>("chatId"), chatId)
            )
            q =
                DbUtils.session.createQuery(user.query)
        }
        return q?.uniqueResult()
    }

    fun deleteUser(commit: Boolean = true, id: Int) {

        DbUtils.transaction(commit) {
            val user: UserModels = DbUtils.entityManager.find(UserModels::class.java, id)
            print("--user is :$user")
            DbUtils.session.remove(user)
        }
    }

    fun getUserByEmail(commit: Boolean = true, email: String): UserModels? {
        var q: Query<UserModels>? = null

        DbUtils.transaction(commit) {
            val user = DbUtils.GetQ(UserModels::class.java)

            user.query.select(user.root).where(
                user.builder.equal(user.root.get<Any>("email"), email)
            )
            q = DbUtils.session.createQuery(user.query)

        }
        return q?.uniqueResult()

    }

    fun insertUser(item: UserModels) {
        DbUtils.transaction {
            DbUtils.session.save(item)
        }

    }

    fun getAll(callBack: (MutableList<UserModels>?) -> Unit?) {
        var q: Query<UserModels>? = null

        DbUtils.transaction {
            val user = DbUtils.GetQ(UserModels::class.java)

            user.query.select(
                user.query.from(UserModels::class.java)
            )
            q = DbUtils.session.createQuery(user.query)


        }
        q?.let {
            callBack(it.resultList)
        }
    }

    fun getUserByRole(): MutableList<UserModels>? {
        var q: Query<UserModels>? = null
        DbUtils.transaction {
            val user = DbUtils.GetQ(UserModels::class.java)
            user.query.select(user.root).where(user.builder.equal(user.root.get<Any>("role"), "admin"))
            q =
                DbUtils.session.createQuery(user.query)
        }
        return q?.resultList
    }

    fun updateUserLogin(email: String) {
        DbUtils.transaction {
            val user = DbUtils.GetQ(UserModels::class.java)

            user.queryUpdater.set(user.root.get<Any>("is_login"), true)
                .where(user.builder.equal(user.root.get<Any>("email"), email))

            DbUtils.entityManager.createQuery(user.query).executeUpdate()

        }
    }
}