package com.backup_bot.user

import javax.persistence.*

@Entity
@Table(name = "user_table")
data class UserModels(
    var email: String,
    var password: String,
    var chatId: String,
    var role: String,
    var is_login: Boolean,
    var state: Int,
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = 0
) {
    constructor() : this("", "", "", "", false, 0)
    constructor(chatId: String) : this()
    constructor(chatId: String, state: Int) : this()


}