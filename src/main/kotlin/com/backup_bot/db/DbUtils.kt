package com.backup_bot.db

import com.backup_bot.C
import com.backup_bot.Utils
import com.backup_bot.user.UserModels
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration
import org.hibernate.resource.transaction.spi.TransactionStatus
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import javax.persistence.EntityManager

object DbUtils {
    /**
     *  Create Entity Manager DB
     */
    private lateinit var sessionFactory: SessionFactory
    lateinit var entityManager: EntityManager
    lateinit var session: Session


    /**
     * init
     */
    fun init() {
        Thread {
            println("---DB Init")
//            var layoutPath= Paths.get("perisitence.cfg.xml");
            sessionFactory =
                Configuration()
                    .configure(File("/app/perisitence.cfg.xml"))
                    .addAnnotatedClass(UserModels::class.java)
                    .buildSessionFactory()
            entityManager = sessionFactory.createEntityManager()
            println("---Session Created")
            C.queue.put("DB")

            session = sessionFactory.currentSession
            session = sessionFactory.openSession()

        }.start()

    }


    /**
     * Transaction checker
     */
    fun transaction(commit: Boolean = true, func: () -> Unit) {


            if (!::session.isInitialized)
                return
            try {
                if (!session.isOpen) {
                    session = sessionFactory.openSession()
                }
                if (!session.transaction.isActive) {

                session.beginTransaction()

                }

                func()
                session.transaction.commit()
                session.clear()
                if (session.transaction.status == TransactionStatus.ACTIVE) {


                    session.close()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

    }

    class GetQ<T>(clazzCreator: Class<T>?) {
        val builder = session.criteriaBuilder
        val query = builder.createQuery(clazzCreator)
        val root = query.from(clazzCreator)
        val queryUpdater = builder.createCriteriaUpdate<T>(clazzCreator)

    }


}