package com.backup_bot.bot

import com.backup_bot.user.UserDao
import com.backup_bot.user.UserModels
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.objects.Update


class Bot : TelegramLongPollingBot() {
    init {

    }


    override fun onUpdateReceived(update: Update?) {
        if (update!!.hasMessage() && update.message.hasText()) {
            println(update.message.text)
            UserDao.getUserByChatId(commit = false, chatId = update.message.chatId.toString())?.let {

                println("---User already exist---")

                BotUtils.botStateSwitcher(update.message.text, update = update)


            } ?: kotlin.run {
                println("---User add---")
                UserDao.insertUser(
                    UserModels(
                        password = " ",
                        role = "",
                        is_login = false,
                        email = "",
                        chatId = update.message.chatId.toString(),
                        state = 0
                    )
                )
                BotUtils.botStateSwitcher("0", update)
            }

        }
    }

    override fun getBotUsername(): String = "tele_backup_bot"

    override fun getBotToken(): String = "1224812416:AAHmiIL9IQiW_b8QVqMp6ib0Kg-eGo0SwLk"

}