package com.backup_bot.bot

import com.backup_bot.C.Companion.queue
import com.backup_bot.Utils
import com.backup_bot.db.DbUtils
import com.backup_bot.user.UserDao
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendDocument
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.exceptions.TelegramApiException

object BotUtils {
    private lateinit var bot: Bot


    fun init() {

        Thread {
            println("---Init Bot")
            ApiContextInitializer.init()
            val botsApi: TelegramBotsApi = TelegramBotsApi()
            try {
                println("---Bot Created")
                bot = Bot()
                botsApi.registerBot(bot)
                queue.put("Bot")

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()

    }

    /**
     * Send Message
     */
    fun sendMessage(content: String? = " ", chatId: String) {
        if (!::bot.isInitialized) {
            return
        }
        val message = SendMessage() // Create a SendMessage object with mandatory fields
            .setChatId(chatId.toLong())
            .setText(content)
        try {
            bot.execute(message) // Call method to send the message
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Send File
     */

    fun sendFile(description: String, fileName: String, chatId: String) {
        val send = SendDocument().apply {
            val file = Utils.getRes(fileName)
            print(file.readLines())
            setDocument(file)
            caption = description
            setChatId(chatId.toLong())
        }
        try {
            bot.execute(send)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun botStateSwitcher(command: String, update: Update) {
        println("---command : $command")


        when (command) {

            "0" -> {

                userStateChanger("0", update.message.chatId.toString())
                sendMessage(
                    "menu : \n 1- Update User \n 2- login",
                    update.message.chatId.toString()
                )
            }

            "1" -> {
                userStateChanger("1", update.message.chatId.toString())
                sendMessage("\r if you want back to menu press 0", update.message.chatId.toString())
                sendMessage("Email : ", update.message.chatId.toString())
                userStateChanger("664845468", update.message.chatId.toString())
            }
            //Email Set State
            "664845468" -> {
                UserDao.getUserByChatId(commit = true, chatId = update.message.chatId.toString())?.let {

                    println("---email set ${update.message.text}")
                    it.email = update.message.text

                }
                sendMessage("password : ", update.message.chatId.toString())
                userStateChanger("354564568", update.message.chatId.toString())


            }
            "354564568" -> {
                UserDao.getUserByChatId(commit = true, chatId = update.message.chatId.toString())?.let {

                    println("---password set ${update.message.text}")
                    it.password = update.message.text


                }
                sendMessage("user created now you can login ", update.message.chatId.toString())
                botStateSwitcher("0", update)


            }
            "2" -> {
                sendMessage("Enter Email ", update.message.chatId.toString())
                userStateChanger("12345", update.message.chatId.toString())
            }

            "12345" -> {
                UserDao.getUserByEmail(commit = false, email = update.message.text)?.let { t ->

                    println("---email set ${update.message.text}")
                    UserDao.getUserByChatId(commit = false, chatId = update.message.chatId.toString())?.let {

                        it.email = update.message.text + "0-1-2"
                        sendMessage("Enter password ", update.message.chatId.toString())

                        userStateChanger("147", update.message.chatId.toString())

                    }


                } ?: kotlin.run {
                    sendMessage("user not found", update.message.chatId.toString())
                    botStateSwitcher("0", update)

                }
            }

            "147" -> {


                    UserDao.getUserByChatId(commit = false, chatId = update.message.chatId.toString())?.let { f ->
                            UserDao.getUserByEmail(
                                commit = false,
                                email = f.email.substring(0, (f.email.length - 5))
                            )?.let { t ->

                                    print("--- input message ${update.message.text}")
                                    print("--- real password ${t.password}")
                                    if (t.password.equals(update.message.text)) {
                                        t.email = f.email.substring(0, (f.email.length - 5))
                                        t.is_login = true
                                        t.chatId = f.chatId
                                        UserDao.deleteUser(false, id = f.id!!)
                                        botStateSwitcher("0", update)
                                        sendMessage("your now login ", update.message.chatId.toString())
                                    } else {
                                        sendMessage("password is wrong ", update.message.chatId.toString())
                                        botStateSwitcher("0", update)
                                    }



                            }

                    }?: kotlin.run {
                        sendMessage("User NotFound ", update.message.chatId.toString())
                        botStateSwitcher("0", update)

                    }



            }


            else -> {
                UserDao.getUserByChatId(commit = false, chatId = update.message.chatId.toString())?.let {
                        botStateSwitcher(it.state.toString(), update)
                }
            }
        }
    }

    private fun userStateChanger(state: String, chatId: String) {
        UserDao.getUserByChatId(chatId = chatId)?.let {
                it.state = state.toInt()


        }
    }
}