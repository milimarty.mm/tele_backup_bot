package com.backup_bot

import com.backup_bot.bot.BotUtils
import com.backup_bot.db.DbUtils
import java.io.File

object Utils {
    var servicesStatus:Boolean = false




    /**
     * init
     */
    fun init(){
        println("---Init")
        DbUtils.init()
        BotUtils.init()

    }


    fun getRes(fileName: String): File = File(fileName)


}