package com.backup_bot

import java.io.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


object ZipFileCreator {
    private const val BUFFER = 1024
    fun zipFile(sourceFileLocation: String, destinationFileLocation: String): String {
        var zos: ZipOutputStream? = null
        var bis: BufferedInputStream? = null
        val myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")
        val myDateObj = LocalDateTime.now()
        val formattedDate: String = myDateObj.format(myFormatObj)
        val zipFileLocation = "${destinationFileLocation}backup-$formattedDate.zip"

        try {
            // source file
            val file = File(sourceFileLocation)
            val fis = FileInputStream(file)
            bis = BufferedInputStream(fis, BUFFER)

            // Creating ZipOutputStream
            val fos = FileOutputStream(zipFileLocation)
            zos = ZipOutputStream(fos)

            // ZipEntry --- Here file name can be created using the source file

            val ze = ZipEntry(file.name)
            // Putting zipentry in zipoutputstream

            zos.putNextEntry(ze)
            val data = ByteArray(BUFFER)
            var count: Int
            while (bis.read(data, 0, BUFFER).also { count = it } != -1) {
                zos.write(data, 0, count)
            }
        } catch (ioExp: IOException) {
            println("Error while zipping " + ioExp.message)
        } finally {
            if (zos != null) {
                try {
                    zos.close()
                } catch (e: IOException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }
            if (bis != null) {
                try {
                    bis.close()
                } catch (e: IOException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }
        }
        return zipFileLocation
    }
    fun zipMaker(sourceFileLocation: String, destinationFileLocation: String): String {

        val myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")
        val myDateObj = LocalDateTime.now()
        val formattedDate: String = myDateObj.format(myFormatObj)
        println("---------------------")
        println("create Zip File : $formattedDate ")
        val zipFileLocation = "${destinationFileLocation}backup-$formattedDate.zip"
        val fos = FileOutputStream(zipFileLocation)
        val zipOut = ZipOutputStream(fos)
        val fileToZip = File(sourceFileLocation)

        zipFile(fileToZip, fileToZip.name, zipOut)
        zipOut.close()
        fos.close()
        return destinationFileLocation
    }
    @Throws(IOException::class)
    private fun zipFile(fileToZip: File, fileName: String, zipOut: ZipOutputStream) {
        if (fileToZip.isHidden) {
            return
        }
        if (fileToZip.isDirectory) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(ZipEntry(fileName))
                zipOut.closeEntry()
            } else {
                zipOut.putNextEntry(ZipEntry("$fileName/"))
                zipOut.closeEntry()
            }
            val children = fileToZip.listFiles()
            for (childFile in children) {
                zipFile(childFile, fileName + "/" + childFile.name, zipOut)
            }
            return
        }
        val fis = FileInputStream(fileToZip)
        val zipEntry = ZipEntry(fileName)
        zipOut.putNextEntry(zipEntry)
        val bytes = ByteArray(1024)
        var length: Int
        while (fis.read(bytes).also { length = it } >= 0) {
            zipOut.write(bytes, 0, length)
        }
        fis.close()
    }
}