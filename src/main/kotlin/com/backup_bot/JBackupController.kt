package com.backup_bot

import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*


class JBackupController {
    fun executeCommand(
        databaseName: String,
        databasePassword: String?,
        type: String,
        backupFilePath: File
    ):String {
//        val backupFilePath = File(
//            System.getProperty("com.backup_bot.user.home")
//                    + File.separator + "backup_" + databaseName
//        )
        if (!backupFilePath.exists()) {
            backupFilePath.mkdirs()
        }
        val sdf = SimpleDateFormat("yyyyMMdd")
        val backupFileName = "backup_" + databaseName + "_" + sdf.format(Date()) + ".sql"
        val commands =
            getPgComands(databaseName, backupFilePath, backupFileName, type)
        if (!commands.isEmpty()) {
            try {
                val pb = ProcessBuilder(commands)
                pb.environment()["PGPASSWORD"] = databasePassword
                val process = pb.start()
                BufferedReader(
                    InputStreamReader(process.errorStream)
                ).use { buf ->
                    var line = buf.readLine()
                    while (line != null) {
                        System.err.println(line)
                        line = buf.readLine()
                    }
                }
                process.waitFor()
                process.destroy()
                println("===> Success on $type process.")
            } catch (ex: IOException) {
                println("Exception: $ex")
            } catch (ex: InterruptedException) {
                println("Exception: $ex")
            }
        } else {
            println("Error: Invalid params.")
        }
        return backupFileName
    }

    private fun getPgComands(
        databaseName: String,
        backupFilePath: File,
        backupFileName: String,
        type: String
    ): List<String> {
        val commands = ArrayList<String>()
        when (type) {
            "backup" -> {
                commands.add("E:\\programs\\postgres\\bin\\pg_dump.exe")
                commands.add("-h") //database server host
                commands.add("localhost")
                commands.add("-p") //database server port number
                commands.add("5432")
                commands.add("-U") //connect as specified database com.backup_bot.user
                commands.add("postgres")
                commands.add("-F") //output file format (custom, directory, tar, plain text (default))
                commands.add("c")
                commands.add("-b") //include large objects in dump
                commands.add("-v") //verbose mode
                commands.add("-f") //output file or directory name
                commands.add(backupFilePath.absolutePath + File.separator + backupFileName)
                commands.add("-d") //database name
                commands.add(databaseName)
            }
            "restore" -> {
                commands.add("pg_restore")
                commands.add("-h")
                commands.add("localhost")
                commands.add("-p")
                commands.add("5432")
                commands.add("-U")
                commands.add("postgres")
                commands.add("-d")
                commands.add(databaseName)
                commands.add("-v")
                commands.add(backupFilePath.absolutePath + File.separator + backupFileName)
            }
            else -> return Collections.EMPTY_LIST as List<String>
        }
        return commands
    }
}