package com.backup_bot

import sun.plugin2.message.Message
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue


class C {
    companion object {
        var queue: BlockingQueue<String> = LinkedBlockingQueue<String>()

        var dbInitStatus: Boolean = false
            set(value) {
                println("--DB SERVICE ONLINE")
                field = value
            }
        var botInitStatus: Boolean = false
            set(value) {
                println("--BOT SERVICE ONLINE")
                field = value
            }

    }
}